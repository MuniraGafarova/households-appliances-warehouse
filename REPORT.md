# Report

##  Name: Households appliances 

### Author: Munira Gaffarova

| TASK                         | Start Date | End Date    |
|------------------------------|------------|-------------|
| Get Info about the Project   | 05.05.2023 | 05.05.2023  |
| Create Project               | 06.05.2023 | 08.05.2023  |
| Create Entity Design         | 08.05.2023 | 08.05.2023  |
| Optimize code Entity Design  | 09.05.2023 | 11.05.202   |
| Create Data Access Layer     | 11.05.2023 | 12.05.2023  |
| Optimize Data Access Layer   | 12.05.2023 | 13.05.2023  |
| Create Service Layer         | 13.05.2023 | 14.05.2023  |
| Optimize code Service Layer  | 14.05.2023 | 15.05.2023  |
| Create Controller Layer      | 15.05.2023 | 15.05.2023  |
| Optimize Controller Layer    | 16.05.2023 | 18.05.2023  |
| Create Repository on Gitlab  | 18.05.2023 | 18.05.2023  |
| Push codes on Repository     | 18.05.2023 | 18.05.2023  |
| Testing                      | 18.05.2023 | 19.05.2023  |   
| Optimize code & Fixing errors| 19.05.2023 | 25.05.2023  |     
| Final Testing                | 25.05.2023 | 26.05.2023  |        
| Create & edit Readme & Report| 30.05.2023 | 03.06.2023  |  
| Prepare the Presantation     | 03.06.2023 | 03.06.2023  |  
| End Project                  | 03.06.2023 | 03.06.2023  |                  
                     


