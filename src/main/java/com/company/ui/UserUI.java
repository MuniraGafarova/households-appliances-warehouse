package com.company.ui;

import com.company.controller.OvenController;
import com.company.controller.RefrigeratorController;
import com.company.controller.TVController;
import com.company.utils.BaseUtils;

import java.util.Objects;
public class UserUI {

    private final TVController tvController = new TVController();
    private final RefrigeratorController refrigeratorController = new RefrigeratorController();
    private final OvenController ovenController = new OvenController();

    public void run() {
        BaseUtils.println("\n\n1 -> TV");
        BaseUtils.println("2 -> Oven");
        BaseUtils.println("3 -> Refrigerator");
        BaseUtils.println("q -> Quit");

        BaseUtils.print("-- Select operation: ");
        switch (BaseUtils.readText()) {
            case "1" -> tvUI();
            case "2" -> ovenUI();
            case "3" -> refrigeratorUI();
            case "q" -> System.exit(0);
            default -> BaseUtils.println("Wrong choice!");
        }
        run();
    }

    public String baseUI() {
        BaseUtils.println("1 -> Show all");
        BaseUtils.println("2 -> Find by id");
        BaseUtils.println("3 -> Find by color");
        BaseUtils.println("4 -> Filter by price");
        BaseUtils.println("0 -> Back");

        BaseUtils.print("Select operation: ");
        return BaseUtils.readText();
    }

    private String showUI() {
        BaseUtils.println("\n\n1 -> Sort by id");
        BaseUtils.println("2 -> Sort by price");
        BaseUtils.println("0 -> Back");

        BaseUtils.print("-- Select operation: ");
        return BaseUtils.readText();
    }

    private void tvUI() {
        BaseUtils.println("\n5 -> Find by volume");
        BaseUtils.println("6 -> Find by brightness");
        BaseUtils.println("7 -> Find by size");
        switch (baseUI()) {
            case "1" -> showAllTV();
            case "2" -> tvController.findByID();
            case "3" -> tvController.findByColor();
            case "4" -> tvController.filterByPrice();
            case "5" -> tvController.findByVolume();
            case "6" -> tvController.findByBrightness();
            case "7" -> tvController.findBySize();
            case "0" -> run();
            default -> BaseUtils.println("Wrong choice!");
        }
        tvUI();
    }

    private void showAllTV() {
        String operation = showUI();
        if (Objects.equals(operation, "0")) {
            tvUI();
        }
        tvController.showAll(operation);
        showAllTV();
    }

    private void ovenUI() {
        BaseUtils.println("\n5 -> Find by temperature");
        BaseUtils.println("6 -> Find by capacity");
        BaseUtils.println("7 -> Find by powerConsumption");
        switch (baseUI()) {
            case "1" -> showAllOven();
            case "2" -> ovenController.findByID();
            case "3" -> ovenController.findByColor();
            case "4" -> ovenController.filterByPrice();
            case "5" -> ovenController.findByTemperature();
            case "6" -> ovenController.findByCapacity();
            case "7" -> ovenController.findByPowerConsumption();
            case "0" -> run();
            default -> BaseUtils.println("Wrong choice!");
        }
        ovenUI();
    }

    private void showAllOven() {
        String operation = showUI();
        if (Objects.equals(operation, "0")) {
            ovenUI();
        }
        ovenController.showAll(operation);
        showAllTV();
    }

    private void refrigeratorUI() {
        BaseUtils.println("\n5 -> Find by capacity");
        BaseUtils.println("6 -> Find by height and weight");
        switch (baseUI()) {
            case "1" -> showAllRefrigerator();
            case "2" -> refrigeratorController.findByID();
            case "3" -> refrigeratorController.findByColor();
            case "4" -> refrigeratorController.filterByPrice();
            case "5" -> refrigeratorController.findByCapacity();
            case "6" -> refrigeratorController.findByHeightWeight();
            case "0" -> run();
            default -> BaseUtils.println("Wrong choice!");
        }
        refrigeratorUI();
    }

    private void showAllRefrigerator() {
        String operation = showUI();
        if (Objects.equals(operation, "0")) {
            refrigeratorUI();
        }
        refrigeratorController.showAll(operation);
        showAllRefrigerator();
    }


}
