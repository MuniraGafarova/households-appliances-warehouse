package com.company.dao;

import com.company.config.CustomFileReader;
import com.company.domains.TV;

import java.io.IOException;
import java.util.List;

public class TVDao implements BaseDao<TV> {
    private final CustomFileReader fileReader = new CustomFileReader();

    @Override
    public List<TV> showAll(String sort) throws IOException {
        return fileReader.readTVFile();
    }

    @Override
    public TV findByID(String id) throws IOException {
        return fileReader.readTVFile()
                .stream().filter(tv -> tv.getId().equals(Long.parseLong(id))).findFirst().orElse(null);
    }

    @Override
    public List<TV> findByColor(String color) throws IOException {
        return fileReader.readTVFile()
                .stream().filter(tv -> tv.getColor().equalsIgnoreCase(color)).toList();
    }

    @Override
    public List<TV> filterByPrice(String min, String max) throws IOException {
        return fileReader.readTVFile()
                .stream().filter(tv ->
                        tv.getPrice() >= Double.parseDouble(min) && tv.getPrice() <= Double.parseDouble(max)).toList();
    }

    public List<TV> findByVolume(String volume) throws IOException {
        return fileReader.readTVFile()
                .stream().filter(tv -> tv.getVolume().equals(Integer.parseInt(volume))).toList();
    }

    public List<TV> findByBrightness(String brightness) throws IOException {
        return fileReader.readTVFile()
                .stream().filter(tv -> tv.getBrightness().equals(Integer.parseInt(brightness))).toList();
    }

    public List<TV> findBySize(String size) throws IOException {
        return fileReader.readTVFile()
                .stream().filter(tv -> tv.getSize().equals(Double.parseDouble(size))).toList();
    }
}
