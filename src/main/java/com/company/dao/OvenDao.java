package com.company.dao;

import com.company.config.CustomFileReader;
import com.company.domains.Oven;

import java.io.IOException;
import java.util.List;

public class OvenDao implements BaseDao<Oven> {

    private final CustomFileReader fileReader = new CustomFileReader();

    @Override
    public List<Oven> showAll(String sort) throws IOException {
        return fileReader.readOvenFile();
    }

    @Override
    public Oven findByID(String id) throws IOException {
        return fileReader.readOvenFile()
                .stream().filter(oven -> oven.getId().equals(Long.parseLong(id))).findFirst().orElse(null);
    }

    @Override
    public List<Oven> findByColor(String color) throws IOException {
        return fileReader.readOvenFile()
                .stream().filter(oven -> oven.getColor().equalsIgnoreCase(color)).toList();
    }

    @Override
    public List<Oven> filterByPrice(String min, String max) throws IOException {
        return fileReader.readOvenFile()
                .stream().filter(oven ->
                        oven.getPrice() >= Double.parseDouble(min) && oven.getPrice() <= Double.parseDouble(max)).toList();
    }

    public List<Oven> findByTemperature(String temperature) throws IOException {
        return fileReader.readOvenFile()
                .stream().filter(oven -> oven.getTemperature().equals(Double.parseDouble(temperature))).toList();
    }

    public List<Oven> findByCapacity(String capacity) throws IOException {
        return fileReader.readOvenFile()
                .stream().filter(oven -> oven.getCapacity().equals(Integer.parseInt(capacity))).toList();
    }

    public List<Oven> findByPowerConsumption(String powerConsumption) throws IOException {
        return fileReader.readOvenFile()
                .stream().filter(oven -> oven.getPowerConsumption().equals(Integer.parseInt(powerConsumption))).toList();
    }
}
