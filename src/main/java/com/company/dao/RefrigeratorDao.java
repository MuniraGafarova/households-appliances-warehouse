package com.company.dao;

import com.company.config.CustomFileReader;
import com.company.domains.Refrigerator;

import java.io.IOException;
import java.util.List;
public class RefrigeratorDao implements BaseDao<Refrigerator> {

    private final CustomFileReader fileReader = new CustomFileReader();

    @Override
    public List<Refrigerator> showAll(String sort) throws IOException {
        return fileReader.readRefrigeratorFile();
    }

    @Override
    public Refrigerator findByID(String id) throws IOException {
        return fileReader.readRefrigeratorFile()
                .stream().filter(refrigerator -> refrigerator.getId().equals(Long.parseLong(id))).findFirst().orElse(null);
    }

    @Override
    public List<Refrigerator> findByColor(String color) throws IOException {
        return fileReader.readRefrigeratorFile()
                .stream().filter(refrigerator -> refrigerator.getColor().equalsIgnoreCase(color)).toList();
    }

    @Override
    public List<Refrigerator> filterByPrice(String min, String max) throws IOException {
        return fileReader.readRefrigeratorFile()
                .stream().filter(refrigerator ->
                        refrigerator.getPrice() >= Double.parseDouble(min) && refrigerator.getPrice() <= Double.parseDouble(max)).toList();
    }

    public List<Refrigerator> findByCapacity(String capacity) throws IOException {
        return fileReader.readRefrigeratorFile()
                .stream().filter(refrigerator -> refrigerator.getCapacity().equals(Integer.parseInt(capacity))).toList();
    }

    public List<Refrigerator> findByHeightWeight(String height, String weight) throws IOException {
        return fileReader.readRefrigeratorFile().stream().filter(refrigerator ->
                refrigerator.getHeight().equals(Double.parseDouble(height)) && refrigerator.getWeight().equals(Double.parseDouble(weight))).toList();
    }
}
