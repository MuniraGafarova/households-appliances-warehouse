package com.company.domains;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Refrigerator extends BaseDomain {
    private Integer capacity;
    private Double height;
    private Double weight;

    @Builder(builderMethodName = "childBuilder")
    public Refrigerator(Long id, String color, Double price, String creationDate,
                        Integer quantity, Integer capacity, Double height, Double weight) {
        super(id, color, price, creationDate, quantity);
        this.capacity = capacity;
        this.height = height;
        this.weight = weight;
    }
}
