package com.company;

import com.company.ui.UserUI;
import com.company.utils.BaseUtils;

public class Main {

    static UserUI userUI = new UserUI();

    public static void main(String[] args) {
        BaseUtils.println("\n\n*************** Project: Warehouse search system *****************");
        BaseUtils.println("--------------- Author: Munira Gaffarova ---------------");
        BaseUtils.println("--------------- Email: Munira_Gafforova@student.itpu.uz ---------------");
        BaseUtils.println("--------------- Creation date: since 05/05/23 17:12 ---------------");
        BaseUtils.println("--------------- Version: version-0.0.1 ---------------");
        userUI.run();
    }
}