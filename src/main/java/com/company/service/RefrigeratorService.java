package com.company.service;

import com.company.dao.RefrigeratorDao;
import com.company.domains.Refrigerator;
import com.company.dtos.AppErrorDTO;
import com.company.dtos.DataDTO;
import com.company.dtos.ResponseEntity;
import com.company.exceptions.GenericNotFoundException;

import java.util.Comparator;
import java.util.List;

public class RefrigeratorService implements BaseService<Refrigerator> {

    private final RefrigeratorDao dao = new RefrigeratorDao();

    @Override
    public ResponseEntity<DataDTO<List<Refrigerator>>> findAll(String sort) {
        try {
            List<Refrigerator> refrigerators = dao.showAll(sort);
            if (refrigerators.isEmpty()) {
                throw new GenericNotFoundException("Refrigerator not found!");
            }
            switch (sort) {
                case "1" -> refrigerators.sort(Comparator.comparing(Refrigerator::getId));
                case "2" -> refrigerators.sort(Comparator.comparing(Refrigerator::getPrice));
            }
            return new ResponseEntity<>(new DataDTO<>(refrigerators));
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<Refrigerator>> findByID(String id) {
        try {
            Refrigerator refrigerator = dao.findByID(id);
            if (refrigerator == null) {
                throw new GenericNotFoundException("Refrigerator not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(refrigerator), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Refrigerator>>> findByColor(String color) {
        try {
            List<Refrigerator> refrigerators = dao.findByColor(color);
            if (refrigerators.isEmpty()) {
                throw new GenericNotFoundException("Refrigerator not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(refrigerators), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Refrigerator>>> filterByPrice(String min, String max) {
        try {
            List<Refrigerator> refrigerators = dao.filterByPrice(min, max);
            if (refrigerators.isEmpty()) {
                throw new GenericNotFoundException("Refrigerator not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(refrigerators), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<Refrigerator>>> findByCapacity(String capacity) {
        try {
            List<Refrigerator> refrigerators = dao.findByCapacity(capacity);
            if (refrigerators.isEmpty()) {
                throw new GenericNotFoundException("Refrigerator not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(refrigerators), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<Refrigerator>>> findByHeightWeight(String height, String weight) {
        try {
            List<Refrigerator> refrigerators = dao.findByHeightWeight(height, weight);
            if (refrigerators.isEmpty()) {
                throw new GenericNotFoundException("Refrigerator not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(refrigerators), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }
}
