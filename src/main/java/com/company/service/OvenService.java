package com.company.service;

import com.company.dao.OvenDao;
import com.company.domains.Oven;
import com.company.dtos.AppErrorDTO;
import com.company.dtos.DataDTO;
import com.company.dtos.ResponseEntity;
import com.company.exceptions.GenericNotFoundException;

import java.util.Comparator;
import java.util.List;

public class OvenService implements BaseService<Oven> {

    private final OvenDao dao = new OvenDao();

    @Override
    public ResponseEntity<DataDTO<List<Oven>>> findAll(String sort) {
        try {
            List<Oven> ovens = dao.showAll(sort);
            if (ovens.isEmpty()) {
                throw new GenericNotFoundException("Oven not found!");
            }
            switch (sort) {
                case "1" -> ovens.sort(Comparator.comparing(Oven::getId));
                case "2" -> ovens.sort(Comparator.comparing(Oven::getPrice));
            }
            return new ResponseEntity<>(new DataDTO<>(ovens));
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<Oven>> findByID(String id) {
        try {
            Oven oven = dao.findByID(id);
            if (oven == null) {
                throw new GenericNotFoundException("Oven not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(oven), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Oven>>> findByColor(String color) {
        try {
            List<Oven> ovens = dao.findByColor(color);
            if (ovens.isEmpty()) {
                throw new GenericNotFoundException("Oven not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(ovens), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Oven>>> filterByPrice(String min, String max) {
        try {
            List<Oven> ovens = dao.filterByPrice(min, max);
            if (ovens.isEmpty()) {
                throw new GenericNotFoundException("Ovens not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(ovens), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<Oven>>> findByTemperature(String temperature) {
        try {
            List<Oven> ovens = dao.findByTemperature(temperature);
            if (ovens.isEmpty()) {
                throw new GenericNotFoundException("Ovens not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(ovens), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<Oven>>> findByCapacity(String capacity) {
        try {
            List<Oven> ovens = dao.findByCapacity(capacity);
            if (ovens.isEmpty()) {
                throw new GenericNotFoundException("Ovens not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(ovens), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<Oven>>> findByPowerConsumption(String powerConsumption) {
        try {
            List<Oven> ovens = dao.findByPowerConsumption(powerConsumption);
            if (ovens.isEmpty()) {
                throw new GenericNotFoundException("Ovens not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(ovens), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }
}
