package com.company.service;

import com.company.dao.TVDao;
import com.company.domains.TV;
import com.company.dtos.AppErrorDTO;
import com.company.dtos.DataDTO;
import com.company.dtos.ResponseEntity;
import com.company.exceptions.GenericNotFoundException;

import java.util.Comparator;
import java.util.List;
public class TVService implements BaseService<TV> {

    private final TVDao dao = new TVDao();

    @Override
    public ResponseEntity<DataDTO<List<TV>>> findAll(String sort) {
        try {
            List<TV> tvs = dao.showAll(sort);
            if (tvs.isEmpty()) {
                throw new GenericNotFoundException("TV not found!");
            }
            switch (sort) {
                case "1" -> tvs.sort(Comparator.comparing(TV::getId));
                case "2" -> tvs.sort(Comparator.comparing(TV::getPrice));
            }
            return new ResponseEntity<>(new DataDTO<>(tvs));
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<TV>> findByID(String id) {
        try {
            TV tv = dao.findByID(id);
            if (tv == null) {
                throw new GenericNotFoundException("TV not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(tv), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<TV>>> findByColor(String color) {
        try {
            List<TV> tvs = dao.findByColor(color);
            if (tvs.isEmpty()) {
                throw new GenericNotFoundException("TV not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(tvs), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<TV>>> filterByPrice(String min, String max) {
        try {
            List<TV> tvs = dao.filterByPrice(min, max);
            if (tvs.isEmpty()) {
                throw new GenericNotFoundException("TV not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(tvs), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<TV>>> findByVolume(String volume) {
        try {
            List<TV> tvs = dao.findByVolume(volume);
            if (tvs.isEmpty()) {
                throw new GenericNotFoundException("TV not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(tvs), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<TV>>> findByBrightness(String brightness) {
        try {
            List<TV> tvs = dao.findByBrightness(brightness);
            if (tvs.isEmpty()) {
                throw new GenericNotFoundException("TV not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(tvs), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<TV>>> findBySize(String size) {
        try {
            List<TV> tvs = dao.findBySize(size);
            if (tvs.isEmpty()) {
                throw new GenericNotFoundException("TV not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(tvs), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }
}
