package com.company.config;

import com.company.domains.Oven;
import com.company.domains.Refrigerator;
import com.company.domains.TV;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CustomFileReader {
    private final String tvPath = "src/main/resources/tv.csv";
    private final String ovenPath = "src/main/resources/oven.csv";
    private final String refrigeratorPath = "src/main/resources/refrigerator.csv";

    public List<TV> readTVFile() throws IOException {
        List<TV> tvs = new ArrayList<>();
        List<String> strings = read(tvPath);
        strings.forEach(s -> tvs.add(toTV(s)));
        return tvs;
    }

    private TV toTV(String line) {
        String[] strings = line.split(",");
        return TV.childBuilder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .price(Double.valueOf(strings[2]))
                .creationDate(strings[3])
                .quantity(Integer.valueOf(strings[4]))
                .volume(Integer.valueOf(strings[5]))
                .brightness(Integer.valueOf(strings[6]))
                .size(Double.valueOf(strings[7]))
                .viewingDistance(Integer.valueOf(strings[8]))
                .build();
    }

    public List<Oven> readOvenFile() throws IOException {
        List<Oven> ovens = new ArrayList<>();
        List<String> strings = read(ovenPath);
        strings.forEach(s -> ovens.add(toOven(s)));
        return ovens;
    }

    private Oven toOven(String line) {
        String[] strings = line.split(",");
        return Oven.childBuilder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .price(Double.valueOf(strings[2]))
                .creationDate(strings[3])
                .quantity(Integer.valueOf(strings[4]))
                .temperature(Double.valueOf(strings[5]))
                .processTime(Integer.valueOf(strings[6]))
                .capacity(Integer.valueOf(strings[7]))
                .powerConsumption(Integer.valueOf(strings[8]))
                .build();

    }

    public List<Refrigerator> readRefrigeratorFile() throws IOException {
        List<Refrigerator> refrigerators = new ArrayList<>();
        List<String> strings = read(refrigeratorPath);
        strings.forEach(s -> refrigerators.add(toRefrigerator(s)));
        return refrigerators;
    }

    private Refrigerator toRefrigerator(String line) {
        String[] strings = line.split(",");
        return Refrigerator.childBuilder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .price(Double.valueOf(strings[2]))
                .creationDate(strings[3])
                .quantity(Integer.valueOf(strings[4]))
                .capacity(Integer.valueOf(strings[5]))
                .height(Double.valueOf(strings[6]))
                .weight(Double.valueOf(strings[7]))
                .build();
    }


    private List<String> read(String path) throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get(path));
        return br.lines().skip(1).toList();
    }
}
