package com.company.controller;

import com.company.domains.Oven;
import com.company.dtos.DataDTO;
import com.company.dtos.ResponseEntity;
import com.company.service.OvenService;
import com.company.utils.BaseUtils;

import java.util.List;
public class OvenController implements BaseController {

    private final OvenService service = new OvenService();

    @Override
    public void showAll(String sort) {
        ResponseEntity<DataDTO<List<Oven>>> responseEntity = service.findAll(sort);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByID() {
        BaseUtils.print("Enter id: ");
        ResponseEntity<DataDTO<Oven>> responseEntity = service.findByID(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByColor() {
        BaseUtils.print("Enter color: ");
        ResponseEntity<DataDTO<List<Oven>>> responseEntity = service.findByColor(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void filterByPrice() {
        BaseUtils.print("Enter min: ");
        String min = BaseUtils.readText();
        BaseUtils.print("Enter max: ");
        String max = BaseUtils.readText();
        ResponseEntity<DataDTO<List<Oven>>> responseEntity = service.filterByPrice(min, max);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByTemperature() {
        BaseUtils.print("Enter temperature: ");
        ResponseEntity<DataDTO<List<Oven>>> responseEntity = service.findByTemperature(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByCapacity() {
        BaseUtils.print("Enter capacity: ");
        ResponseEntity<DataDTO<List<Oven>>> responseEntity = service.findByCapacity(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByPowerConsumption() {
        BaseUtils.print("Enter powerConsumption: ");
        ResponseEntity<DataDTO<List<Oven>>> responseEntity = service.findByPowerConsumption(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }
}
