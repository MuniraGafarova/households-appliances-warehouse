package com.company.controller;

import com.company.domains.TV;
import com.company.dtos.DataDTO;
import com.company.dtos.ResponseEntity;
import com.company.service.TVService;
import com.company.utils.BaseUtils;

import java.util.List;

public class TVController implements BaseController {
    private final TVService service = new TVService();

    @Override
    public void showAll(String sort) {
        ResponseEntity<DataDTO<List<TV>>> responseEntity = service.findAll(sort);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByID() {
        BaseUtils.print("Enter id: ");
        ResponseEntity<DataDTO<TV>> responseEntity = service.findByID(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByColor() {
        BaseUtils.print("Enter color: ");
        ResponseEntity<DataDTO<List<TV>>> responseEntity = service.findByColor(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void filterByPrice() {
        BaseUtils.print("Enter min: ");
        String min = BaseUtils.readText();
        BaseUtils.print("Enter max: ");
        String max = BaseUtils.readText();
        ResponseEntity<DataDTO<List<TV>>> responseEntity = service.filterByPrice(min, max);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByVolume() {
        BaseUtils.print("Enter volume: ");
        ResponseEntity<DataDTO<List<TV>>> responseEntity = service.findByVolume(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByBrightness() {
        BaseUtils.print("Enter brightness: ");
        ResponseEntity<DataDTO<List<TV>>> responseEntity = service.findByBrightness(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findBySize() {
        BaseUtils.print("Enter size: ");
        ResponseEntity<DataDTO<List<TV>>> responseEntity = service.findBySize(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }
}
