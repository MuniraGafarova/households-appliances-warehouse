package com.company.controller;

import com.company.domains.Refrigerator;
import com.company.dtos.DataDTO;
import com.company.dtos.ResponseEntity;
import com.company.service.RefrigeratorService;
import com.company.utils.BaseUtils;

import java.util.List;

public class RefrigeratorController implements BaseController {

    private final RefrigeratorService service = new RefrigeratorService();

    @Override
    public void showAll(String sort) {
        ResponseEntity<DataDTO<List<Refrigerator>>> responseEntity = service.findAll(sort);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByID() {
        BaseUtils.print("Enter id: ");
        ResponseEntity<DataDTO<Refrigerator>> responseEntity = service.findByID(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByColor() {
        BaseUtils.print("Enter color: ");
        ResponseEntity<DataDTO<List<Refrigerator>>> responseEntity = service.findByColor(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void filterByPrice() {
        BaseUtils.print("Enter min: ");
        String min = BaseUtils.readText();
        BaseUtils.print("Enter max: ");
        String max = BaseUtils.readText();
        ResponseEntity<DataDTO<List<Refrigerator>>> responseEntity = service.filterByPrice(min, max);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByCapacity() {
        BaseUtils.print("Enter capacity: ");
        ResponseEntity<DataDTO<List<Refrigerator>>> responseEntity = service.findByCapacity(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    public void findByHeightWeight() {
        BaseUtils.print("Enter height: ");
        String height = BaseUtils.readText();
        BaseUtils.print("Enter weight: ");
        String weight = BaseUtils.readText();
        ResponseEntity<DataDTO<List<Refrigerator>>> responseEntity = service.findByHeightWeight(height, weight);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }
}
