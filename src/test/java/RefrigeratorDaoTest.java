import com.company.dao.RefrigeratorDao;
import com.company.domains.Refrigerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class RefrigeratorDaoTest {
    private final RefrigeratorDao dao = new RefrigeratorDao();

    @Test
    public void findAllTest() throws IOException {
        String sort = "1";
        List<Refrigerator> refrigerators = dao.showAll(sort);
        Assertions.assertFalse(refrigerators.isEmpty(), "Find all method tested!");
    }

    @Test
    public void findByIDTest() throws IOException {
        String id = "3";
        Refrigerator refrigerator = dao.findByID(id);
        Assertions.assertEquals(refrigerator.getId(), Long.parseLong(id), "Find by id method tested!");
    }

    @Test
    public void findByColorTest() throws IOException {
        String color = "white";
        List<Refrigerator> refrigerators = dao.findByColor(color);
        boolean isTrue = true;
        for (Refrigerator refrigerator : refrigerators) {
            if (!refrigerator.getColor().equalsIgnoreCase(color)) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void filterByPriceTest() throws IOException {
        String max = "15";
        String min = "30";
        List<Refrigerator> refrigerators = dao.filterByPrice(min, max);
        boolean isTrue = true;
        for (Refrigerator refrigerator : refrigerators) {
            if (refrigerator.getQuantity() >= Integer.parseInt(min) && refrigerator.getQuantity() <= Integer.parseInt(max)) {
                isTrue = false;
            }
        }
        Assertions.assertTrue(isTrue, "Filter by price method tested!");
    }

    @Test
    public void findByCapacityTest() throws IOException {
        String capacity = "200";
        List<Refrigerator> refrigerators = dao.findByColor(capacity);
        boolean isTrue = true;
        for (Refrigerator refrigerator : refrigerators) {
            if (!refrigerator.getCapacity().equals(Integer.parseInt(capacity))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void findByHeightWeight() throws IOException {
        String height = "3";
        String weight = "35";
        List<Refrigerator> refrigerators = dao.findByHeightWeight(height, weight);
        boolean isTrue = true;
        for (Refrigerator refrigerator : refrigerators) {
            if (!refrigerator.getHeight().equals(Double.parseDouble(height)) || !refrigerator.getWeight().equals(Double.parseDouble(weight))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }
}
