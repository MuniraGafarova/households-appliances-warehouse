import com.company.dao.OvenDao;
import com.company.domains.Oven;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class OvenDaoTest {
    private final OvenDao dao = new OvenDao();

    @Test
    public void findAllTest() throws IOException {
        String sort = "1";
        List<Oven> oven = dao.showAll(sort);
        Assertions.assertFalse(oven.isEmpty(), "Find all method tested!");
    }

    @Test
    public void findByIDTest() throws IOException {
        String id = "3";
        Oven oven = dao.findByID(id);
        Assertions.assertEquals(oven.getId(), Long.parseLong(id), "Find by id method tested!");
    }

    @Test
    public void findByColorTest() throws IOException {
        String color = "black";
        List<Oven> ovens = dao.findByColor(color);
        boolean isTrue = true;
        for (Oven oven : ovens) {
            if (!oven.getColor().equalsIgnoreCase(color)) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void filterByPriceTest() throws IOException {
        String max = "15";
        String min = "30";
        List<Oven> ovens = dao.filterByPrice(min, max);
        boolean isTrue = true;
        for (Oven oven : ovens) {
            if (oven.getQuantity() >= Integer.parseInt(min) && oven.getQuantity() <= Integer.parseInt(max)) {
                isTrue = false;
            }
        }
        Assertions.assertTrue(isTrue, "Filter by price method tested!");
    }

    @Test
    public void findByTemperatureTest() throws IOException {
        String temperature = "30";
        List<Oven> ovens = dao.findByColor(temperature);
        boolean isTrue = true;
        for (Oven oven : ovens) {
            if (!oven.getTemperature().equals(Double.parseDouble(temperature))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void findByCapacityTest() throws IOException {
        String capacity = "15";
        List<Oven> ovens = dao.findByCapacity(capacity);
        boolean isTrue = true;
        for (Oven oven : ovens) {
            if (!oven.getCapacity().equals(Integer.parseInt(capacity))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void findByPowerConsumptionTest() throws IOException {
        String powerConsumption = "4800";
        List<Oven> ovens = dao.findByColor(powerConsumption);
        boolean isTrue = true;
        for (Oven oven : ovens) {
            if (!oven.getPowerConsumption().equals(Integer.parseInt(powerConsumption))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }
}
