import com.company.dao.TVDao;
import com.company.domains.TV;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class TVDaoTest {
    private final TVDao dao = new TVDao();

    @Test
    public void findAllTest() throws IOException {
        String sort = "1";
        List<TV> tv = dao.showAll(sort);
        Assertions.assertFalse(tv.isEmpty(), "Find all method tested!");
    }

    @Test
    public void findByIDTest() throws IOException {
        String id = "3";
        TV tv = dao.findByID(id);
        Assertions.assertEquals(tv.getId(), Long.parseLong(id), "Find by id method tested!");
    }

    @Test
    public void findByColorTest() throws IOException {
        String color = "grey";
        List<TV> tvs = dao.findByColor(color);
        boolean isTrue = true;
        for (TV tv : tvs) {
            if (!tv.getColor().equalsIgnoreCase(color)) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void filterByPriceTest() throws IOException {
        String max = "15";
        String min = "30";
        List<TV> tvs = dao.filterByPrice(min, max);
        boolean isTrue = true;
        for (TV tv : tvs) {
            if (tv.getQuantity() >= Integer.parseInt(min) && tv.getQuantity() <= Integer.parseInt(max)) {
                isTrue = false;
            }
        }
        Assertions.assertTrue(isTrue, "Filter by price method tested!");
    }

    @Test
    public void findByVolumeTest() throws IOException {
        String volume = "30";
        List<TV> tvs = dao.findByVolume(volume);
        boolean isTrue = true;
        for (TV tv : tvs) {
            if (!tv.getVolume().equals(Integer.parseInt(volume))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void findByBrightness() throws IOException {
        String brightness = "100";
        List<TV> tvs = dao.findByBrightness(brightness);
        boolean isTrue = true;
        for (TV tv : tvs) {
            if (!tv.getBrightness().equals(Integer.parseInt(brightness))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }

    @Test
    public void findBySizeTest() throws IOException {
        String size = "1.5";
        List<TV> tvs = dao.findBySize(size);
        boolean isTrue = true;
        for (TV tv : tvs) {
            if (!tv.getSize().equals(Double.parseDouble(size))) {
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue, "Find by color method tested!");
    }
}
